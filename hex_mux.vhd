library ieee;
use ieee.std_logic_1164.all;

entity hex_mux is 
port (
	concat            : in std_logic_vector(7 downto 0);
	sum 					: in std_logic_vector(7 downto 0);
	mux_select			: in std_logic_vector(3 downto 0);
	hex_out				: out std_logic_vector(7 downto 0)
);

end entity hex_mux;


architecture mux_logic of hex_mux is 


begin 

with mux_select select
hex_out <= concat when "0000",
			  concat when "0001",
			  concat when "0010",
			  concat when "0100",
			  sum when "1000",
			  "10001000" when others;
			  

end mux_logic;