library ieee;
use ieee.std_logic_1164.all;

entity logical_processor is 
port (
		in_1					: in std_logic_vector(3 downto 0);
		in_2 					: in std_logic_vector(3 downto 0);
		pb_select			: in std_logic_vector(3 downto 0);
		sum					: in std_logic_vector(7 downto 0);
		out_1					: out std_logic_vector(7 downto 0)
);

end entity logical_processor;


architecture logical_processor_logic of logical_processor is 


begin 

with pb_select select
out_1 <=  ("0000" & (in_1 AND in_2)) when "0001",
				("0000" & (in_1 OR in_2)) when "0010",
				("0000" & (in_1 XOR in_2)) when "0100",
				sum when "1000",
				"00000000" when "0000",
				"11111111" when others;

end logical_processor_logic;