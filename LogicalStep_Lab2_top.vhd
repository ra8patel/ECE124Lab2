library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity LogicalStep_Lab2_top is port (
   clkin_50			: in	std_logic;
	pb					: in	std_logic_vector(3 downto 0);
 	sw   				: in  std_logic_vector(7 downto 0); -- The switch inputs
   leds				: out std_logic_vector(7 downto 0); -- for displaying the switch content
   seg7_data 		: out std_logic_vector(6 downto 0); -- 7-bit outputs to a 7-segment
	seg7_char1  	: out	std_logic;				    		-- seg7 digit1 selector
	seg7_char2  	: out	std_logic				    		-- seg7 digit2 selector
	
); 
end LogicalStep_Lab2_top;

architecture SimpleCircuit of LogicalStep_Lab2_top is
--
-- Components Used ---
------------------------------------------------------------------- 
  component SevenSegment port (
   hex   		:  in  std_logic_vector(3 downto 0);   -- The 4 bit data to be displayed
   sevenseg 	:  out std_logic_vector(6 downto 0)    -- 7-bit outputs to a 7-segment
   ); 
   end component;
	
	component segment7_mux port (
		clk	: in std_logic := '0';
		DIN2	: in std_logic_vector(6 downto 0);
		DIN1	: in std_logic_vector(6 downto 0);
		DOUT	: out std_logic_vector(6 downto 0);
		DIG2 	: out std_logic;
		DIG1 	: out std_logic
	);
	end component;
	
component hex_mux port (

	concat            : in std_logic_vector(7 downto 0);
	sum 					: in std_logic_vector(7 downto 0);
	mux_select			: in std_logic_vector(3 downto 0);
	hex_out				: out std_logic_vector(7 downto 0)

	);
	end component;
	component logical_processor port (
		in_1					: in std_logic_vector(3 downto 0);
		in_2 					: in std_logic_vector(3 downto 0);
		pb_select			: in std_logic_vector(3 downto 0);
		sum					: in std_logic_vector(7 downto 0);
		out_1					: out std_logic_vector(7 downto 0)
	);
	end component;
	
-- Create any signals, or temporary variables to be used
--
--  std_logic_vector is a signal which can be used for logic operations such as OR, AND, NOT, XOR
--
	signal seg7_A		: std_logic_vector(6 downto 0);
	signal hex_A		: std_logic_vector(3 downto 0);
	signal seg7_B		: std_logic_vector(6 downto 0);
	signal hex_B		: std_logic_vector(7 downto 4);
	signal pb_bar 		: std_logic_vector(3 downto 0);
	signal hex_concat	: std_logic_vector(7 downto 0);
	signal sum 			: std_logic_vector(7 downto 0);
	signal out_seg7	: std_logic_vector(7 downto 0);
	--signal DIN2 		: std_logic_vector(6 downto 0);
	--signal DIN1 		: std_logic_vector(6 downto 0);
	--signal clock 		: clkin_50;
	
	
-- Here the circuit begins

begin

pb_bar <= not(pb);

hex_A <= sw(3 downto 0);
--DIN2 <= seg7_A (6 downto 0);
--DIN1 <= seg7_B (6 downto 0);
--seg7_data <= seg7_A;
hex_B <= sw(7 downto 4);

sum <= std_logic_vector(unsigned("0000" & hex_A) + unsigned("0000" & hex_B));


hex_concat <= hex_B & hex_A;
-- NOTE : MIGHT WANA SWITCH

--seg7_data <= seg7_B;
--seg7_A <= seg7_data(6 downto 0);

--COMPONENT HOOKUP
--
-- generate the seven segment coding
	
	
	INST1: SevenSegment port map(out_seg7(3 downto 0), seg7_A);
	
	INST2: SevenSegment port map(out_seg7(7 downto 4), seg7_B);
		
	INST3: segment7_mux port map(clkin_50, seg7_A, seg7_B, seg7_data, seg7_char2, seg7_char1);
	
	INST4: hex_mux port map(hex_concat, sum, pb_bar, out_seg7);
	
	INST5: logical_processor port map(hex_A, hex_B, pb_bar, sum, leds); 

 
end SimpleCircuit;

